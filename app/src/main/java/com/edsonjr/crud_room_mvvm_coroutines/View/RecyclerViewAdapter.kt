package com.edsonjr.crud_room_mvvm_coroutines.View

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.edsonjr.crud_room_mvvm_coroutines.R
import com.edsonjr.crud_room_mvvm_coroutines.db.Subscriber

class RecyclerViewAdapter(private val subscribersList: List<Subscriber>,
    private val clickLIstener:(Subscriber)->Unit):
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {



    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val nameEdit = view.findViewById<TextView>(R.id.subscriber_RecyclerItem_name)
        val emailEdit = view.findViewById<TextView>(R.id.subscriber_RecyclerItem_email)

        fun bind(item: Subscriber,clickLIstener:(Subscriber)->Unit) {
            nameEdit.text = item.name
            emailEdit.text = item.email
            itemView.setOnClickListener {
                clickLIstener(item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var layoutInflater = LayoutInflater.from(parent.context).inflate(R.layout.list_item,parent,false)
        return ViewHolder(layoutInflater)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(subscribersList.get(position),clickLIstener)
    }


    override fun getItemCount() = subscribersList.size


}