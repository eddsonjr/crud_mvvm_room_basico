package com.edsonjr.crud_room_mvvm_coroutines.View

import com.edsonjr.crud_room_mvvm_coroutines.db.Subscriber
import java.util.concurrent.Flow

interface RecyclerViewClickListener {

    fun clickListener(subscriber: Subscriber)
}