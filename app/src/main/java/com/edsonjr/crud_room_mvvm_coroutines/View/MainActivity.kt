package com.edsonjr.crud_room_mvvm_coroutines.View

import android.os.Binder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.edsonjr.crud_room_mvvm_coroutines.R
import com.edsonjr.crud_room_mvvm_coroutines.ViewModel.SubscbriberViewModel
import com.edsonjr.crud_room_mvvm_coroutines.ViewModel.SubscriberViewModelFactory
import com.edsonjr.crud_room_mvvm_coroutines.databinding.ActivityMainBinding
import com.edsonjr.crud_room_mvvm_coroutines.db.Subscriber
import com.edsonjr.crud_room_mvvm_coroutines.db.SubscriberDataBase
import com.edsonjr.crud_room_mvvm_coroutines.db.SubscriberRepository

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var subscbriberViewModel: SubscbriberViewModel
    private val TAG = "[MainActivity]"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupViewModel() //configura a parte de viewmodel
        initRecyclerView() //inicializar a recyclerview



        //acoa do botao de salvar/update
        binding.btnSaveUpdateSubscriber.setOnClickListener {
            subscbriberViewModel.inputName.value = binding.editName.text.toString()
            subscbriberViewModel.inputEmail.value = binding.editEmail.text.toString()

            //salvando dados no banco
            subscbriberViewModel.saveOrUpdate()
            clearEditFields()
        }



        //acao do botao de deleteAll
        binding.btnDeleteAll.setOnClickListener {
            subscbriberViewModel.clearAllOrDelete() //removendo todos os dados do banco
            clearEditFields()
        }
    }


    override fun onResume() {
        super.onResume()
        displaySubscribesList()
    }



    //metodo pra fazer a configuracao do dao, viewModel e repository
    private fun setupViewModel() {
        val dao = SubscriberDataBase.getInstance(application).subscriberDAO
        val repository = SubscriberRepository(dao)
        val factory = SubscriberViewModelFactory(repository)
        subscbriberViewModel = ViewModelProvider(this,factory).get(SubscbriberViewModel::class.java)

    }


    //metodo para atualizar a recyclerview com base nos dados que estao vindo via viewmodel
    private fun displaySubscribesList() {
        subscbriberViewModel.subscribers.observe(this, Observer { subscriberslist ->
            Log.i(TAG,"Subscribers qt: ${subscriberslist.size}")
            binding.subscriberRecyclerview.adapter = RecyclerViewAdapter(subscriberslist,
                {selectedItem:Subscriber->itemClickeListener(selectedItem)})

        })

    }


    //metodo para trabalhar  inicializar a recyclerview
    private fun initRecyclerView() {
        binding.subscriberRecyclerview.layoutManager = LinearLayoutManager(this)
    }


    //metodo para trabalhar com os eventos de click da recyclerview
    private fun itemClickeListener(subscriber: Subscriber){
        Toast.makeText(this,"Subscriber: ${subscriber.name} selected",Toast.LENGTH_SHORT).show()
        subscbriberViewModel.initUpdateAndDelete(subscriber)
        updateUIButtons(subscriber)

    }


    //funcao para fazer update dos elementos da UI, quando um item da recyclerview for selecionado
    private fun updateUIButtons(subscriber: Subscriber) {
        binding.btnSaveUpdateSubscriber.text = "Update"
        binding.btnDeleteAll.text = "Delete"
        binding.editName.setText(subscriber.name)
        binding.editEmail.setText(subscriber.email)
    }


    //esta funcao serve para apagar os dados dos campos de texto
    private fun clearEditFields() {
        binding.editName.text.clear()
        binding.editEmail.text.clear()
        binding.editName.requestFocus()

    }




}