package com.edsonjr.crud_room_mvvm_coroutines.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


//Esta classe e usada pelo ROOM e serve para definir uma entidade do banco de dados
@Entity(tableName = "subscriber_data_table")
data class Subscriber(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "subscriber_id")
    val id: Int,

    @ColumnInfo(name = "subscriber_name")
    val name: String,


    @ColumnInfo(name = "subscriber_email")
    val email: String

)