package com.edsonjr.crud_room_mvvm_coroutines.ViewModel

import android.database.Observable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.edsonjr.crud_room_mvvm_coroutines.db.Subscriber
import com.edsonjr.crud_room_mvvm_coroutines.db.SubscriberRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.*

class SubscbriberViewModel(private val repository: SubscriberRepository): ViewModel(){


    val subscribers = repository.subscribers
    private var isUpdateOrDelete = false
    private lateinit var subscriberToUpdateOrDelete: Subscriber

    var inputName = MutableLiveData<String?>()
    var inputEmail = MutableLiveData<String?>()
    val saveOrUpdateButtonText = MutableLiveData<String>()
    val clearAllOrDeleteButtonText = MutableLiveData<String>()



    fun saveOrUpdate() {
        val name = inputName.value!!
        val email = inputEmail.value!!
        insert(Subscriber(0,name,email))

        inputEmail.value = null
        inputName.value = null

    }

    fun clearAllOrDelete() {
        if(isUpdateOrDelete) {
            delete(subscriberToUpdateOrDelete) //ira remover somente o subscriber selecionado
        }else {
            clearAll()
        }

    }

    fun insert(subscriber: Subscriber): Job = viewModelScope.launch{
            repository.insert(subscriber)
    }

    fun udpate(subscriber: Subscriber): Job = viewModelScope.launch {
        repository.update(subscriber)
    }

    fun delete(subscriber: Subscriber): Job = viewModelScope.launch {
        repository.delete(subscriber)
        isUpdateOrDelete = false

    }

    fun clearAll(): Job = viewModelScope.launch {
        repository.deleteAll()
    }



    //esta funcoa e chamada para alterar o estado do item selecionado para um possivel update
    //ou possivel remocao
    fun initUpdateAndDelete(subscriber: Subscriber){
        isUpdateOrDelete = true
        subscriberToUpdateOrDelete = subscriber
    }
}


