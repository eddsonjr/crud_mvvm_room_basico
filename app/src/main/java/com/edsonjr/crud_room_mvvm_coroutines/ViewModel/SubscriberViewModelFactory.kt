package com.edsonjr.crud_room_mvvm_coroutines.ViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.edsonjr.crud_room_mvvm_coroutines.db.SubscriberRepository
import java.lang.IllegalArgumentException

class SubscriberViewModelFactory(private val repository: SubscriberRepository): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(SubscbriberViewModel::class.java)){
            return SubscbriberViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknow View Model class")

    }

}